//
//  game.hpp
//  hangman
//
//  Created by Maggie on 2019. 9. 13..
//  Copyright © 2019 Maggie. All rights reserved.
//
#pragma warning(disable : 4200)
#ifndef game_hpp
#define game_hpp

#include <stdio.h>
#include <string>
#include <array>
#include <functional>

class hangman
{
public:
    hangman();
    ~hangman();
    
public:
    void show_menu();

private:
    void start_game_loop();
	bool set_theme(size_t _theme = 0);
    void init_game();
    std::string get_secret_word();
    bool is_running() const;
    void input();
    void update();
	void render() const;
	void print_myword() const;
	void print_keys() const;
	void print_status() const;
	void print_hangman0() const;
	void print_hangman1() const;
	void print_hangman2() const;
	void print_hangman3() const;
	void print_hangman4() const;
	void print_hangman5() const;
	void print_hangman6() const;
	std::vector<std::function<void(void)>> print_hangman;
    
private:
	size_t theme = 0;
    bool running = true;
    
	std::string secret_word;
    std::string my_word;
	char char_guessed{};

	const size_t MAX_CHANCE = 6;
    size_t hit = 0;
    size_t chance = MAX_CHANCE;
    
    enum game_status {PLAYING, WIN, LOSE};
    game_status status = PLAYING;

	std::array<bool, 26> alphabet_available{};
    
private:
	std::vector<std::string> words;

private:
	hangman(const hangman&) = delete;
	hangman& operator=(const hangman&) = delete;
};

#endif /* game_hpp */
