//
//  game.cpp
//  hangman
//
//  Created by Maggie on 2019. 9. 13..
//  Copyright © 2019 Maggie. All rights reserved.
//

#include "game.hpp"
#include <iostream>
#include <cctype> // toupper
#include <random> // random
#include <fstream>

hangman::hangman()
{
	const size_t size = MAX_CHANCE + 1;
	print_hangman.reserve(size);
	print_hangman.emplace_back(std::move([&]() {print_hangman0(); }));
	print_hangman.emplace_back(std::move([&]() {print_hangman1(); }));
	print_hangman.emplace_back(std::move([&]() {print_hangman2(); }));
	print_hangman.emplace_back(std::move([&]() {print_hangman3(); }));
	print_hangman.emplace_back(std::move([&]() {print_hangman4(); }));
	print_hangman.emplace_back(std::move([&]() {print_hangman5(); }));
	print_hangman.emplace_back(std::move([&]() {print_hangman6(); }));
}

hangman::~hangman()
{
}

void hangman::show_menu()
{
	while (true)
	{
		system("cls");
		std::cout << "Choose a theme\n";
		std::cout << "1: Counturies\n";
		std::cout << "2: Fruits\n";
		std::cout << "0: Exit this game\n";
		std::cin >> theme;

		if (theme == 0)
		{
			std::cout << "\n";
			std::cout << "GAME OVER\n";
			std::cout << "Thank you for playing\n";
			break;
		}
		else if (set_theme(theme))
		{
			start_game_loop();
		}
	}
}

void hangman::start_game_loop()
{
    system("cls");
    init_game();
    
    while (is_running())
    {
        input();
        update();
        render();
    }
}

bool hangman::set_theme(size_t _theme)
{
	theme = _theme;
	std::ifstream file;
	switch (theme)
	{
	case 1:
		file.open("..\\..\\dictionary\\counturies.txt");
		break;
	case 2:
		file.open("..\\..\\dictionary\\fruits.txt");
		break;
	}

	if (file.is_open() == false)
	{
		std::cout << "Failed to open theme\n";
		std::cout << "Please any key to retry\n";
		std::cin.ignore();
		std::cin.ignore();
		return false;
	}

	const auto lines = std::count(std::istreambuf_iterator<char>(file),
		std::istreambuf_iterator<char>(), '\n');
	words.reserve(lines + 1);

	std::string s;
	file.seekg(0, std::ios::beg);
	while (file.eof() == false)
	{
		getline(file, s);
		words.emplace_back(s);
	}
	
	file.close();

	return true;
}

void hangman::init_game()
{
    secret_word = get_secret_word();
    hit = 0;
	chance = MAX_CHANCE;
    running = true;
    status = PLAYING;
    my_word.resize(secret_word.size());
    
    for (auto & _1 : my_word)
    {
        _1 = '_';
    }

	for (auto& _1 : alphabet_available)
	{
		_1 = true;
	}
    
    render();
}

std::string hangman::get_secret_word()
{
    static std::random_device rd;
	static std::mt19937_64 gen(rd());
    static std::uniform_int_distribution<size_t> dist(0, words.size() - 1);
    
    const auto index = dist(gen);

	std::string selected = words[index];

	for (auto& _1 : selected)
	{
		_1 = toupper(_1);
	}
    
    return selected;
}

bool hangman::is_running() const
{
    return running;
}

void hangman::input()
{
    std::cout << "Enter a letter you guessed\n";
    
    std::cin >> char_guessed;
    char_guessed = toupper(char_guessed);
}

void hangman::update()
{
    if (std::string::npos != my_word.find(char_guessed))
    {
        return;
    }

	if (char_guessed < 'A' || char_guessed > 'Z')
	{
		return;
	}

	const size_t index_alphabet = static_cast<size_t>(char_guessed) - 'A';
	if (alphabet_available[index_alphabet] == false)
	{
		return;
	}

	alphabet_available[index_alphabet] = false;
    
    size_t pos = -1;
    int count_hit = 0;
    do
    {
        pos = secret_word.find(char_guessed, pos + 1);
    
        if (pos == std::string::npos)
        {
        }
        else
        {
            ++count_hit;
            my_word.at(pos) = char_guessed;
        }
    }
    while(pos != std::string::npos);
    
    if (count_hit == 0)
    {
        --chance;
    }
    else
    {
        hit += count_hit;
    }

    if (chance == 0)
    {
        status = LOSE;
		running = false;
    }
    else if (hit == secret_word.size())
    {
        status = WIN;
		running = false;
    }
}

void hangman::render() const
{
    system("cls");

	print_hangman[chance]();
	print_myword();
	print_keys();
	print_status();
}

void hangman::print_myword() const
{
	for (const auto& _1 : my_word)
	{
		std::cout << _1 << " ";
	}

	std::cout << "\n";

	std::cout << "Charaters: " << my_word.length() << "\n";
}

void hangman::print_keys() const
{
	std::cout << "\n";

	for (size_t i = 'A'; i <= 'Z'; ++i)
	{
		if (alphabet_available[i - 'A'])
		{
			std::cout << static_cast<char>(i) << " ";
		}
		else
		{
			std::cout << "  ";
		}

		if (i == 'M')
		{
			std::cout << "\n";
		}
	}
	std::cout << "\n\n";
}

void hangman::print_status() const
{
	if (status == WIN)
	{
		std::cout << "Player wins\n\n";
		std::cout << "Press any key to continue\n";
		std::cin.ignore();
		std::cin.ignore();
	}
	else if (status == LOSE)
	{
		std::cout << "Host wins\n";
		std::cout << "The secret word was " << secret_word << "\n\n";
		std::cout << "Press any key to continue\n";
		std::cin.ignore();
		std::cin.ignore();
	}
}

void hangman::print_hangman0() const
{
	std::cout << "  -----\n";
	std::cout << "  |   |\n";
	std::cout << "  |   O\n";
	std::cout << "  |  /|\\\n";
	std::cout << "  |  / \\\n";
	std::cout << "  |\n";
	std::cout << "  |\\\n";
	std::cout << "-------\n";
}

void hangman::print_hangman1() const
{
	std::cout << "  -----\n";
	std::cout << "  |   |\n";
	std::cout << "  |   O\n";
	std::cout << "  |  /|\\\n";
	std::cout << "  |  /\n";
	std::cout << "  |\n";
	std::cout << "  |\\\n";
	std::cout << "-------\n";
}

void hangman::print_hangman2() const
{
	std::cout << "  -----\n";
	std::cout << "  |   |\n";
	std::cout << "  |   O\n";
	std::cout << "  |  /|\\\n";
	std::cout << "  |\n";
	std::cout << "  |\n";
	std::cout << "  |\\\n";
	std::cout << "-------\n";
}

void hangman::print_hangman3() const
{
	std::cout << "  -----\n";
	std::cout << "  |   |\n";
	std::cout << "  |   O\n";
	std::cout << "  |  /|\n";
	std::cout << "  |\n";
	std::cout << "  |\n";
	std::cout << "  |\\\n";
	std::cout << "-------\n";
}

void hangman::print_hangman4() const
{
	std::cout << "  -----\n";
	std::cout << "  |   |\n";
	std::cout << "  |   O\n";
	std::cout << "  |  /\n";
	std::cout << "  |\n";
	std::cout << "  |\n";
	std::cout << "  |\\\n";
	std::cout << "-------\n";
}

void hangman::print_hangman5() const
{
	std::cout << "  -----\n";
	std::cout << "  |   |\n";
	std::cout << "  |   O\n";
	std::cout << "  |\n";
	std::cout << "  |\n";
	std::cout << "  |\n";
	std::cout << "  |\\\n";
	std::cout << "-------\n";
}

void hangman::print_hangman6() const
{
	std::cout << "  -----\n";
	std::cout << "  |   |\n";
	std::cout << "  |\n";
	std::cout << "  |\n";
	std::cout << "  |\n";
	std::cout << "  |\n";
	std::cout << "  |\\\n";
	std::cout << "-------\n";
}