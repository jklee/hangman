//
//  main.cpp
//  hangman
//
//  Created by Maggie on 2019. 9. 13..
//  Copyright © 2019 Maggie. All rights reserved.
//

#include <iostream>
#include "game.hpp"

int main(int argc, const char * argv[])
{
    hangman game;
    game.show_menu();
    
    return 0;
}
